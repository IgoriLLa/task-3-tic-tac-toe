let gameField = document.getElementById('game-field'); 
let move = 0; // определяет кто сейчас ходит
let result = '';
const modalWindow = document.getElementById('content');
const resitWindow = document.getElementById('modal-window');
const closwWindow = document.getElementById('btn-close-window');
const resetButton = document.getElementById('reset-button');

//обработчик событий клика
gameField.addEventListener('click', e => {
    if (e.target.className = 'game-field_cell') {
        move % 2 === 0 ? e.target.innerHTML = 'x' : e.target.innerHTML = 'o';
        move++;
        check(); 
    }
})

//функция проверки победителя
const check = () => {
    const gameFieldCells = document.getElementsByClassName('game-field_cell');

    const array = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];

    for (i = 0; i < array.length; i++) {
        if (gameFieldCells[array[i][0]].innerHTML == 'x' && gameFieldCells[array[i][1]].innerHTML == 'x' && gameFieldCells[array[i][2]].innerHTML == 'x') {
            result = 'крестики';
            outputResult(result);
        } else if (gameFieldCells[array[i][0]].innerHTML == 'o' && gameFieldCells[array[i][1]].innerHTML == 'o' && gameFieldCells[array[i][2]].innerHTML == 'o') {
            result = 'нолики';
            outputResult(result);
        }else if (move >= 9){
            result = 'Ничья';
            outputResultIfDraw(result);
        }

    }
}

const outputResultIfDraw = draw => {
    modalWindow.innerHTML = `${draw} !!`;
    resitWindow.style.display = 'block';
}

const outputResult = winner => {
    modalWindow.innerHTML = `Победили ${winner} !!`;
    resitWindow.style.display = 'block';
}

const closeWindowMet = () => {
    resitWindow.style.display = 'none';
}

const newGame = () =>{
    location.reload();
}

closwWindow.addEventListener('click', closeWindowMet);
resetButton.addEventListener('click', newGame);